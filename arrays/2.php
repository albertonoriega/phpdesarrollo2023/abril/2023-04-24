<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php

    //Array asociativo inicializado

    $numeroVocales = [
        'a' => 10,
        'e' => 1,
        'i' => 0,
        'o' => 5,
        'u' => 0,
    ];

    // Leer el indice 'a'

    $numeroVocales['a']; //10

    ?>
</body>

</html>