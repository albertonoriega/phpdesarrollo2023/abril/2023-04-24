<?php

// Calcular el numero de vocales que tiene el texto introducido
$vocales = [
    'a' => 0,
    'e' => 0,
    'i' => 0,
    'o' => 0,
    'u' => 0,
];
$contarA = 0;
$contarE = 0;
$contarI = 0;
$contarO = 0;
$contarU = 0;
$contarVocales = 0;

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php

    // Comprobamos si se ha enviado un texto
    if (isset($_GET["enviar"])) {
        $texto = $_GET["texto"];
        //Pasamos el texto a minúsculas
        $texto = strtolower($texto);

        // Iteramos el string y para cada caracter comprobamos si es una vocal
        for ($i = 0; $i < strlen($texto); $i++) {
            // comprobamos si cada carácter de texto en una 'a' 
            if ($texto[$i] == 'a') {
                //si es una 'a' sumamos uno a la variable contarA
                $contarA++;
                //Tambien se puede hacer $vocales['a']++;
                //si es una 'a' sumamos uno a la variable contarVocales
                $contarVocales++;
            }
            if ($texto[$i] == 'e') {
                $contarE++;
                $contarVocales++;
            }
            if ($texto[$i] == 'i') {
                $contarI++;
                $contarVocales++;
            }
            if ($texto[$i] == 'o') {
                $contarO++;
                $contarVocales++;
            }
            if ($texto[$i] == 'u') {
                $contarU++;
                $contarVocales++;
            }
        }

        //Asignamos el numero de vocales al array vocales
        $vocales['a'] = $contarA;
        $vocales['e'] = $contarE;
        $vocales['i'] = $contarI;
        $vocales['o'] = $contarO;
        $vocales['u'] = $contarU;

        //Imprimimos el texto introducido
        echo ("Texto introducido: {$texto} <br>");

        // Imprimimos el array vocales
        foreach ($vocales as $indice => $valor) {
            echo ("Número de {$indice}: $valor<br>");
        }
        //Imprimimos el numero total de vocales
        echo ("Número total de vocales: {$contarVocales}");


        //cargamos el formulario al final de la página
        cargarFormulario();
    } else {
        //Si no ha pulsado enviar que cargue el formulario de nuevo
        cargarFormulario();
    }


    //Función que te carga el formulario
    function cargarFormulario()
    {

    ?>
        <form action="">
            <div>
                <label for="texto">Introducir texto</label>
                <input type="text" name="texto" id="texto">
            </div>
            <div>
                <button name="enviar">Calcular</button>
            </div>
        </form>
    <?php
    }
    ?>


</body>

</html>