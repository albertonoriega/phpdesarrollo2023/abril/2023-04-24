<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    //Ejemplo con foreach

    $numeros = [1, 2, 35, 87, 24]; // array enumerado
    $letras = ['a', 's', 'j'];

    $notas = [
        'Jorge' => 10,
        'César' => 6,
        'Eva' => 9,
    ]; // array asociativo

    //Quiero mostrar el array numeros
    //foreach
    foreach ($numeros as $indice => $valor) {
        echo "{$valor}<br>";
    }
    //for
    for ($i = 0; $i < count($numeros); $i++) {
        echo "{$numeros[$i]}<br>";
    }

    foreach ($letras as $valor) {
        echo "{$valor}<br>";
    }

    foreach ($notas as $indice => $valor) {
        echo "{$indice}:  {$valor}<br>";
    }

    ?>
</body>

</html>